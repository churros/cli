# Generated by ariadne-codegen on 2023-10-28 17:06
# Source: https://churros.inpt.fr/graphql

from enum import Enum


class CredentialType(str, Enum):
    Password = "Password"
    Token = "Token"


class DocumentType(str, Enum):
    CourseNotes = "CourseNotes"
    CourseSlides = "CourseSlides"
    Exam = "Exam"
    Exercises = "Exercises"
    GradedExercises = "GradedExercises"
    Miscellaneous = "Miscellaneous"
    Practical = "Practical"
    PracticalExam = "PracticalExam"
    Summary = "Summary"


class EventFrequency(str, Enum):
    Biweekly = "Biweekly"
    Monthly = "Monthly"
    Once = "Once"
    Weekly = "Weekly"


class GroupType(str, Enum):
    Association = "Association"
    Club = "Club"
    Group = "Group"
    Integration = "Integration"
    List = "List"
    StudentAssociationSection = "StudentAssociationSection"


class LogoSourceType(str, Enum):
    ExternalLink = "ExternalLink"
    GroupLogo = "GroupLogo"
    Icon = "Icon"
    InternalLink = "InternalLink"


class NotificationChannel(str, Enum):
    Articles = "Articles"
    Comments = "Comments"
    GodparentRequests = "GodparentRequests"
    GroupBoard = "GroupBoard"
    Other = "Other"
    Permissions = "Permissions"
    Shotguns = "Shotguns"


class PaymentMethod(str, Enum):
    Card = "Card"
    Cash = "Cash"
    Check = "Check"
    Lydia = "Lydia"
    Other = "Other"
    Transfer = "Transfer"


class RegistrationVerificationState(str, Enum):
    AlreadyVerified = "AlreadyVerified"
    NotFound = "NotFound"
    NotPaid = "NotPaid"
    Ok = "Ok"
    Opposed = "Opposed"
    OtherEvent = "OtherEvent"


class Visibility(str, Enum):
    GroupRestricted = "GroupRestricted"
    Private = "Private"
    Public = "Public"
    SchoolRestricted = "SchoolRestricted"
    Unlisted = "Unlisted"
